Laboratory work #2 for Programming for Parallel Computer Systems.

Purpose for the work: To study the Semaphore, Mutex, Critical Section and Event mechanism, that was made in program library for Windows - Win32.
Programming language: C++.
Used technologies: From the library <Windows.h> was used some functions and procedures to work with the threads. Also, to stand with the objects that represents mechanisms for that work, there was implemented some special types for them (HANDLE, CRITICAL_SECTION, DWORD).

Controling of the program:
variable N stands for the size of Matrixes and Vectors respectively.
Program works for 4 threads, first one outputs the result.