//------------------------------PfPCS---------------------------------------
//----------------------------Labwork #2------------------------------------
//-------WinAPI.Semaphores, Mutexes, Events, Critical Sections--------------
//--------------------------------------------------------------------------
//--------------------Task: A = sort(B)*d + e * T*(MO*MK)-------------------
//--------------------------------------------------------------------------
//---- - Author : Butskiy Yuriy, IO - 52 group------------------------------
//---- - Date : 28.03.2018--------------------------------------------------
//--------------------------------------------------------------------------

#include "data.h"
#include <iostream>


void input_Integer(int &a)
{
	a = 1;
}

void input_Vector(Vector &A)
{
	for (int i = 0; i < N; i++)
	{
		A[i] = 1;
	}
}

void input_Matrix(Matrix &MA)
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			MA[i][j] = 1;
		}
	}
}

void output_Vector(const Vector &A)
{
	if (N < 10)
	{
		std::cout << std::endl;
		for (int i = 0; i < N; i++)
		{
			std::cout << A[i] << " ";
		}
		std::cout << std::endl;
	}
}

Vector multiply_Matrix_Matrix_Vector(const Matrix &MA, const Matrix &MB, const Vector &A, const int k)
{
	int cell;
	Matrix matr(N, N);
	Vector result(N);
	for (int i = H * k; i < H*(1 + k); i++)
	{
		for (int j = 0; j < N; j++)
		{
			cell = 0;
			for (int k = 0; k < N; k++)
			{
				cell += MA[i][k] * MB[k][j];
			}
			matr[i][j] = cell;
		}
		cell = 0;
		for (int j = 0; j < N; j++)
		{
			cell += A[i] * matr[i][j];
		}
		result[i] = cell;
	}
	return result;
}

Vector multiply_Vector_Integer(const Vector &A, const int a, const int k)
{
	Vector result(N);
	for (int i = H * k; i < H*(1 + k); i++)
	{
		result[i] = A[i] * a;
	}
	return result;
}

void sum_Vectors(const Vector &A, const Vector &B, Vector &C, const int k)
{
	for (int i = H * k; i < H*(1 + k); i++)
	{
		C[i] = A[i] + B[i];
	}
}

void sort_Vectors(Vector &A, const int left, const int right)
{
	if (left == right)
		return;
	int middle = (left + right) / 2;
	sort_Vectors(A, left, middle);
	sort_Vectors(A, middle + 1, right);

	int i = left;
	int j = middle + 1;
	Vector B(N);
	for (int step = 0; step < right - left + 1; step++)
	{
		if ((j > right) || ((i <= middle) && (A[i] < A[j])))
		{
			B[step] = A[i];
			i++;
		}
		else
		{
			B[step] = A[j];
			j++;
		}
	}
	for (int step = 0; step < right - left + 1; step++)
		A[left + step] = B[step];
}