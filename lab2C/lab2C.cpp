//------------------------------PfPCS---------------------------------------
//----------------------------Labwork #2------------------------------------
//-------WinAPI.Semaphores, Mutexes, Events, Critical Sections--------------
//--------------------------------------------------------------------------
//--------------------Task: A = sort(B)*d + e * T*(MO*MK)-------------------
//--------------------------------------------------------------------------
//---- - Author : Butskiy Yuriy, IO - 52 group------------------------------
//---- - Date : 28.03.2018--------------------------------------------------
//--------------------------------------------------------------------------

#include <iostream>
#include <Windows.h>
#include "data.h"

using namespace std;

int N = 8;
int P = 4;

int d, e;
Vector A(N), B(N), T(N);
Matrix MO(N, N), MK(N, N);

HANDLE Sem_sort1, Sem_sort2, Sem_sort1_1, Sem_end[3], Sem2_134,
Mute, Event1_234, Event3_124, Event4_123;
CRITICAL_SECTION Crit_Sec;

void T1()
{
	int k = 0;
	int e1, d1;
	Matrix MK1(N, N);
	cout << "T1 started" << endl;

	//1. Input values of B and MO
	input_Vector(B);
	input_Matrix(MO);

	//2. Tell T2, T3 and T4 about input B and MO
	SetEvent(Event1_234);
	//3. Wait for input of d and MK
	WaitForSingleObject(Event3_124, INFINITE);
	//4. Wait for input of e and T
	WaitForSingleObject(Event4_123, INFINITE);

	//5. Calculating of local sort
	sort_Vectors(B, k * H, H * (1 + k) - 1);
	//6. Tell T2 about end of local sort
	ReleaseSemaphore(Sem_sort1, 1, NULL);
	//7. Wait T2 for global sort
	WaitForSingleObject(Sem2_134, INFINITE);

	//8. Copying local variables
	EnterCriticalSection(&Crit_Sec);
	e1 = e;
	LeaveCriticalSection(&Crit_Sec);
	WaitForSingleObject(Mute, INFINITE);
	d1 = d;
	MK1 = MK;
	ReleaseMutex(Mute);
	
	//9. Calculating of the algorithm
	sum_Vectors(multiply_Vector_Integer(B, d1, k), multiply_Vector_Integer(multiply_Matrix_Matrix_Vector(MO,MK1,T,k), e1, k), A, k);

	//10. Wait for end of calculations in T2, T3, T4
	WaitForMultipleObjects(3, Sem_end, TRUE, INFINITE);
	//11. Output result
	output_Vector(A);

	cout << "T1 finished" << endl;
}

void T2()
{
	int k = 1;
	int d2, e2;
	Matrix MK2(N, N);
	cout << "T2 started" << endl;

	//1. Wait for input of B and MO
	WaitForSingleObject(Event1_234, INFINITE);
	//2. Wait for input of d and MK
	WaitForSingleObject(Event3_124, INFINITE);
	//3. Wait for input of e and T
	WaitForSingleObject(Event4_123, INFINITE);

	//4. Calcuating of local sort
	sort_Vectors(B, k * H, H * (1 + k) - 1);
	//5. Wait for end of local sort in T1
	WaitForSingleObject(Sem_sort1, INFINITE);
	//6. Calculating of local sort step 2
	sort_Vectors(B, 0, H * (1 + k) - 1);
	//7. Wait for end of local sort step 2 in T4
	WaitForSingleObject(Sem_sort1_1, INFINITE);
	//8. Calculating global sort
	sort_Vectors(B, 0, N - 1);
	//9. Tell T1, T3, T4 about end of global sort
	ReleaseSemaphore(Sem2_134, 3, NULL);

	//10. Copying local variables
	WaitForSingleObject(Mute, INFINITE);
	d2 = d;
	MK2 = MK;
	ReleaseMutex(Mute);
	EnterCriticalSection(&Crit_Sec);
	e2 = e;
	LeaveCriticalSection(&Crit_Sec);

	//11. Calculating of the algorithm
	sum_Vectors(multiply_Vector_Integer(B, d2, k), multiply_Vector_Integer(multiply_Matrix_Matrix_Vector(MO, MK2, T, k), e2, k), A, k);

	//12. Tell T1 about end of calculation
	ReleaseSemaphore(Sem_end[0], 1, NULL);

	cout << "T2 finished" << endl;
}

void T3()
{
	int k = 2;
	int e3, d3;
	Matrix MK3(N, N);
	cout << "T3 started" << endl;

	//1. Input values of d and MK
	input_Integer(d);
	input_Matrix(MK);

	//2. Tell T1, T2, T4 about input of d and MK
	SetEvent(Event3_124);
	//3. Wait for input of B and MO
	WaitForSingleObject(Event1_234, INFINITE);
	//4. Wait for input of e and T
	WaitForSingleObject(Event4_123, INFINITE);

	//5. Calculating of local sort
	sort_Vectors(B, k * H, H* (1 + k) - 1);
	//6. Tell T4 about end of local sort
	ReleaseSemaphore(Sem_sort2, 1, NULL);
	//7. Wait for end of global sort in T2
	WaitForSingleObject(Sem2_134, INFINITE);

	//8. Copying local variables
	EnterCriticalSection(&Crit_Sec);
	e3 = e;
	LeaveCriticalSection(&Crit_Sec);
	WaitForSingleObject(Mute, INFINITE);
	d3 = d;
	MK3 = MK;
	ReleaseMutex(Mute);

	//9. Calculating of the algorithm
	sum_Vectors(multiply_Vector_Integer(B, d3, k), multiply_Vector_Integer(multiply_Matrix_Matrix_Vector(MO, MK3, T, k), e3, k), A, k);

	//10. Tell T1 about end of calculation
	ReleaseSemaphore(Sem_end[1], 1, NULL);

	cout << "T3 finished" << endl;
}

void T4()
{
	int k = 3;
	int d4, e4;
	Matrix MK4(N, N);
	cout << "T4 started" << endl;

	//1. Input values of e end T
	input_Integer(e);
	input_Vector(T);

	//2. Tell T1, T2, T3 about input of e end T
	SetEvent(Event4_123);
	//3. Wait for input of B and MO
	WaitForSingleObject(Event1_234, INFINITE);
	//4. Wait for input of d and MK
	WaitForSingleObject(Event3_124, INFINITE);

	//5. Calculating of local sort
	sort_Vectors(B, k * H, H * (1 + k) - 1);
	//6. Wait T3 for local sort
	WaitForSingleObject(Sem_sort2, INFINITE);
	//7. Calculating of local sort step 2
	sort_Vectors(B, (k - 1) * H, H * (1 + k) - 1);
	//8. Tell T2 about end of local sort step 2
	ReleaseSemaphore(Sem_sort1_1, 1, NULL);
	//9. Wait for end of global sort in T2
	WaitForSingleObject(Sem2_134, INFINITE);

	//10. Copying local variables
	WaitForSingleObject(Mute, INFINITE);
	d4 = d;
	MK4 = MK;
	ReleaseMutex(Mute);
	EnterCriticalSection(&Crit_Sec);
	e4 = e;
	LeaveCriticalSection(&Crit_Sec);

	//11. Calculating of the algorithm
	sum_Vectors(multiply_Vector_Integer(B, d4, k), multiply_Vector_Integer(multiply_Matrix_Matrix_Vector(MO, MK4, T, k), e4, k), A, k);

	//12. Tell T1 about end of calculation
	ReleaseSemaphore(Sem_end[2], 1, NULL);

	cout << "T4 finished" << endl;
}

int main()
{
	cout << "Labwork#2 started" << endl;

	Sem2_134 = CreateSemaphore(NULL, 0, 3, NULL);
	Sem_sort1 = CreateSemaphore(NULL, 0, 1, NULL);
	Sem_sort2 = CreateSemaphore(NULL, 0, 1, NULL);
	Sem_sort1_1 = CreateSemaphore(NULL, 0, 1, NULL);
	Sem_end[0] = CreateSemaphore(NULL, 0, 1, NULL);
	Sem_end[1] = CreateSemaphore(NULL, 0, 1, NULL);
	Sem_end[2] = CreateSemaphore(NULL, 0, 1, NULL);

	Mute = CreateMutex(NULL, FALSE, NULL);

	Event1_234 = CreateEvent(NULL, TRUE, FALSE, NULL);
	Event3_124 = CreateEvent(NULL, TRUE, FALSE, NULL);
	Event4_123 = CreateEvent(NULL, TRUE, FALSE, NULL);

	InitializeCriticalSection(&Crit_Sec);

	DWORD Tid0, Tid1, Tid2, Tid3;
	HANDLE threads[] =
	{
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)T1, NULL, 0, &Tid0),
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)T2, NULL, 0, &Tid1),
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)T3, NULL, 0, &Tid2),
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)T4, NULL, 0, &Tid3)
	};

	WaitForMultipleObjects(4, threads, TRUE, INFINITE);
	CloseHandle(threads[0]);
	CloseHandle(threads[1]);
	CloseHandle(threads[2]);
	CloseHandle(threads[3]);

	cout << "Labwork#2 finished" << endl;
	cin.get();

    return 0;
}

